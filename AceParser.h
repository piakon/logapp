#pragma once
#include "IParser.h"
class AceParser :
	public IParser
{
public:
	Log parse(std::string line);
private:
	void removeSpaces(std::string &str);
};

