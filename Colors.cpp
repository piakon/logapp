#include "Colors.h"

ImVec4 Color::Error = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);
ImVec4 Color::Trace = ImVec4(1.0f, 0.5f, 0.0f, 1.0f);
ImVec4 Color::Debug = ImVec4(0.0f, 1.0f, 1.0f, 1.0f);
ImVec4 Color::Info = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);
ImVec4 Color::Warning = ImVec4(1.0f, 0.0f, 1.0f, 1.0f);