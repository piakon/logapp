#pragma once
#include "imgui/imgui.h"

struct Color{
	static ImVec4 Error;
	 static ImVec4 Trace;
	 static ImVec4 Debug;
	 static ImVec4 Info;
	 static ImVec4 Warning;
};
