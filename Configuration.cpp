#include "Configuration.h"


Configuration::Configuration(std::string filename) : INIReader(filename), m_filename(filename) {
	
}

bool Configuration::save(std::string defaultPath, int port) {

	std::ofstream out;
	out.open(m_filename);
	if (out.is_open()) {

		writeColor(out, Color::Trace, "TRACE");
		writeColor(out, Color::Info, "INFO");
		writeColor(out, Color::Debug, "DEBUG");
		writeColor(out, Color::Warning, "WARNING");
		writeColor(out, Color::Error, "ERROR");

		savePort(out ,port);
		savePath(out, defaultPath);
	
		out.close();
		return true;
	}
	else {
		return false;
	}

}

void Configuration::writeColor(std::ofstream& out, ImVec4 color, std::string name) {
	out << "["<< name <<"] " << "\n";
	out << "RED=" << color.x << "\n";
	out << "GREEN=" << color.y << "\n";
	out << "BLUE=" << color.z << "\n";
}

ImVec4 Configuration::getColor(std::string name) {
	float r, g, b;
	r = GetFloat(name, "RED", 0.0);
	g = GetFloat(name, "GREEN", 0.0);
	b = GetFloat(name, "BLUE", 0.0);
	return ImVec4(r, g, b, 1);
}

void Configuration::setColors() {
	Color::Trace = getColor("TRACE");
	Color::Info = getColor("INFO");
	Color::Debug = getColor("DEBUG");
	Color::Warning = getColor("WARNING");
	Color::Error = getColor("ERROR");
}

void Configuration::savePort(std::ofstream& out, int port) {
	out << "[PORT]" << "\n";
	out << "PORT=" << port << "\n";
}

void Configuration::savePath(std::ofstream& out, std::string defaultPath) {
	out << "[PATH]" << "\n";
	out << "DEFAULT_PATH=" << defaultPath << "\n";
}