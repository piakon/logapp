#pragma once
#include "INIReader.h"
#include <fstream>
#include <iostream>
#include "Colors.h"

class Configuration : public INIReader
{
public:
	Configuration( std::string filename );

	bool save(std::string defaultPath, int port);

	void setColors();


private:
	std::string m_filename;

	void writeColor(std::ofstream& out, ImVec4 color, std::string name);

	ImVec4 getColor(std::string name);

	void savePort(std::ofstream& out, int port);

	void savePath(std::ofstream& out, std::string defaultPath);


};

