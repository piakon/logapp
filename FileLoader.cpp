//
// Created by Ondra on 10.03.2020.
//

#include "FileLoader.h"

FileLoader::FileLoader(std::string path) : ILoader(), m_path(path) {
	thread1 = std::thread(&FileLoader::read, this);
}


FileLoader::~FileLoader() {
	if(thread1.joinable()) thread1.join();
}

void FileLoader::read() {
	std::ifstream m_file(m_path, std::ifstream::in);
	std::string line;
	if (m_file.is_open()) {
		std::cout << "file is Open" << std::endl;
		while (std::getline(m_file, line)) {
			m_mutexVector.lock();
			m_lines.push_back(line);
			m_mutexVector.unlock();
		}
	}
	else {
		std::cout << "cannot open file" << std::endl;
	}
	m_file.close();
}

std::vector<std::string> FileLoader::returnLines() {
	std::vector<std::string> returnlines;
	m_mutexVector.lock();
	returnlines.swap(m_lines);
	m_mutexVector.unlock();
	return returnlines;
}
