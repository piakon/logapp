#ifndef APP_LOADER_H
#define APP_LOADER_H


#include <deque>
#include <fstream>
#include <thread>
#include <mutex>

#include "IParser.h"
#include "HypercomLoggingConstants.h"
#include "ILoader.h"

class FileLoader : public ILoader  {
	std::vector<std::string> m_lines;
public:

	FileLoader(std::string path);
	~FileLoader();

	void read();
	std::vector<std::string> returnLines() ;

private:
	std::string m_path;

	std::thread thread1;

	std::mutex m_mutexVector;


};


#endif //APP_LOADER_H
