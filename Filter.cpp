#include "Filter.h"

char Filter::m_filter1[256] = "";
char Filter::m_filter2[256] = "";
char Filter::m_filter3[256] = "";
char Filter::m_filter4[256] = "";
char Filter::m_filter5[256] = "";
char Filter::m_filter6[256] = "";
char Filter::m_filter7[256] = "";
char Filter::m_filter8[256] = "";
char Filter::m_filter9[256] = "";
char Filter::m_filter10[256] = "";

bool Filter::m_showInfo = true;
bool Filter::m_showTrace = true;
bool Filter::m_showDebug = true;
bool Filter::m_showWarning = true;
bool Filter::m_showError = true;

int Filter::m_numOfTextFilters = 0;

bool Filter::filterBySeverity(Log log) {
	if (std::regex_search(log.level, INFO) && m_showInfo) return true;
	if (std::regex_search(log.level, TRACE) && m_showTrace) return true;
	if (std::regex_search(log.level, DEBUG) && m_showDebug) return true;
	if (std::regex_search(log.level, WARN) && m_showWarning) return true;
	if (std::regex_search(log.level, ERRORR) && m_showError) return true;
	return false;
}

bool Filter::filterByText(int numOfFilterApplied, Log log) {

	if (numOfFilterApplied >= 1) { return checkPattern(m_filter1, log);}
	if (numOfFilterApplied >= 2) { return checkPattern(m_filter2, log); }
	if (numOfFilterApplied >= 3) { return checkPattern(m_filter3, log); }
	if (numOfFilterApplied >= 4) { return checkPattern(m_filter4, log); }
	if (numOfFilterApplied >= 5) { return checkPattern(m_filter5, log); }
	if (numOfFilterApplied >= 6) { return checkPattern(m_filter6, log); }
	if (numOfFilterApplied >= 7) { return checkPattern(m_filter7, log); }
	if (numOfFilterApplied >= 8) { return checkPattern(m_filter8, log); }
	if (numOfFilterApplied >= 9) { return checkPattern(m_filter9, log); }
	if (numOfFilterApplied >= 10) { return checkPattern(m_filter10, log); }
	return false;
}

bool Filter::checkPattern(char text[], Log log) {
	std::regex pattern = std::regex(text, std::regex_constants::icase | std::regex_constants::ECMAScript);
	return std::regex_search(log.returnLog(), pattern);
}
