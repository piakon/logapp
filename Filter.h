#pragma once
#include "Log.h"
#include "SeverityRegexConstants.h"
#include <regex>

class Filter
{
public:
	bool filterBySeverity(Log l);
	bool filterByText(int numOfFilterApplied, Log log);

	bool checkPattern(char text[], Log log);

	static bool m_showInfo;
	static bool m_showTrace;
	static bool m_showDebug;
	static bool m_showWarning;
	static bool m_showError;

	static char m_filter1[256];
	static char m_filter2[256];
	static char m_filter3[256];
	static char m_filter4[256];
	static char m_filter5[256];
	static char m_filter6[256];
	static char m_filter7[256];
	static char m_filter8[256];
	static char m_filter9[256];
	static char m_filter10[256];

	static int m_numOfTextFilters;


private:




};

