#pragma once
#include <string>

const std::string TAG_RECORD_START = "<record>";
const std::string TAG_RECORD_END = "</record>";
const std::string TAG_TYPE_START = "<type>";
const std::string TAG_TYPE_END = "</type>";
const std::string TAG_DATE_START = "<date>";
const std::string TAG_DATE_END = "</date>";
const std::string TAG_LOGGER_START = "<logger>";
const std::string TAG_LOGGER_END = "</logger>";
const std::string TAG_LEVEL_START = "<level>";
const std::string TAG_LEVEL_END = "</level>";
const std::string TAG_CLASS_START = "<class>";
const std::string TAG_CLASS_END = "</class>";
const std::string TAG_FCE_START = "<method>";
const std::string TAG_FCE_END = "</method>";
const std::string TAG_MSG_START = "<message>";
const std::string TAG_MSG_END = "</message>";
const std::string TAG_FILE_START = "<file>";
const std::string TAG_FILE_END = "</file>";
const std::string TAG_LINE_START = "<line>";
const std::string TAG_LINE_END = "</line>";
const std::string TAG_SRCIP_START = "<src_ip>";
const std::string TAG_SRCIP_END = "</src_ip>";
const std::string TAG_LOCTIME_START = "<local_time>";
const std::string TAG_LOCTIME_END = "</local_time>";
const std::string NEW_LINE = "\\r\\n";
const std::string REPLACE_AMPERSAND = "&amp;";
const std::string REPLACE_TAGCHAR1 = "&lt;";
const std::string REPLACE_TAGCHAR2 = "&gt;";