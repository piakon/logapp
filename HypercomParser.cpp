#include "HypercomParser.h"

Log HypercomParser::parse(std::string line) {
	Log newLog;

	newLog.type = getValue(line, TAG_TYPE_START, TAG_TYPE_END);
	newLog.date = getValue(line, TAG_DATE_START, TAG_DATE_END);
	newLog.logger = getValue(line, TAG_LOGGER_START, TAG_LOGGER_END);
	newLog.level = getValue(line, TAG_LEVEL_START, TAG_LEVEL_END);
	newLog.clazz = getValue(line, TAG_CLASS_START, TAG_CLASS_END);
	newLog.method = getValue(line, TAG_FCE_START, TAG_FCE_END);
	newLog.message = getValue(line, TAG_MSG_START, TAG_MSG_END);
	newLog.file = getValue(line, TAG_FILE_START, TAG_FILE_END);
	newLog.line = std::stoi(getValue(line, TAG_LINE_START, TAG_LINE_END));
	newLog.srcIp = getValue(line, TAG_SRCIP_START, TAG_SRCIP_END);
	newLog.localTime = std::stoi(getValue(line, TAG_LOCTIME_START, TAG_LOCTIME_END));

	return newLog;
}

std::string HypercomParser::getValue(std::string line, std::string start, std::string end) {
	unsigned first_delim_pos = line.find(start);
	unsigned end_pos_of_first_delim = first_delim_pos + start.length();
	unsigned last_delim_pos = line.find(end);

	return line.substr(end_pos_of_first_delim,
		last_delim_pos - end_pos_of_first_delim);
}