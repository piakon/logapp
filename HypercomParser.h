
#include "IParser.h"
#include "HypercomLoggingConstants.h"
class HypercomParser :
	public IParser
{
public:
	Log parse(std::string line);
private:
	std::string getValue(std::string line, std::string start, std::string end);
};
