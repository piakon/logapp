#include "HypersomSaver.h"

void HypercomSaver::saveLogs(std::string path, std::deque<Log> logs) {
	std::ofstream file(path);
	if (file.is_open()) {
		for(Log l : logs){
			file << TAG_RECORD_START;
			file << TAG_TYPE_START << l.type << TAG_TYPE_END;
			file << TAG_DATE_START << l.date << TAG_DATE_END;
			file << TAG_LOGGER_START << l.logger << TAG_LOGGER_END;
			file << TAG_LEVEL_START << l.level << TAG_LEVEL_END;
			file << TAG_CLASS_START << l.clazz << TAG_CLASS_END;
			file << TAG_FCE_START << l.method << TAG_FCE_END;
			file << TAG_MSG_START << l.message << TAG_MSG_END;
			file << TAG_FILE_START << l.file << TAG_FILE_END;
			file << TAG_LINE_START << l.line << TAG_LINE_END;
			file << TAG_SRCIP_START << l.srcIp << TAG_SRCIP_END;
			file << TAG_LOCTIME_START << l.localTime << TAG_LOCTIME_END;
			file << TAG_RECORD_END << std::endl;
		}
	}
	file.close();

}

