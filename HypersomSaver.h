#pragma once
#include <iostream>
#include <deque>
#include <fstream>

#include "Log.h"
#include "HypercomLoggingConstants.h"
#include "ISaver.h"

class HypercomSaver : public ISaver
{
public:
	void saveLogs(std::string path, std::deque<Log> logs);
};


