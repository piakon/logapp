#pragma once
#include <vector>

//getting lines from source
class ILoader{
public:
	virtual void read() = 0;
	virtual std::vector<std::string> returnLines() = 0;

};