//
// Created by Ondra on 10.03.2020.
//

#ifndef APP_IPARSER_H
#define APP_IPARSER_H

#include "Log.h"

class IParser {
public:
   virtual Log parse(std::string line) = 0;

};



#endif //APP_IPARSER_H
