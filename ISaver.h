#pragma once
#include <deque>
#include <string>
#include "Log.h"

class ISaver {
public:
	virtual	void saveLogs(std::string path, std::deque<Log> logs) = 0;


};