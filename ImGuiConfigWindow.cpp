#include "ImGuiConfigWindow.h"


bool ImGuiConfigWindow::m_showDefaultPathPicker = false;


ImGuiConfigWindow::ImGuiConfigWindow(Configuration& config):m_defaultPath(config.Get("PATH", "DEFAULT_PATH", ".")) {
	//m_portNumber = config.GetInteger("PORT", "PORT", 0);
}

void ImGuiConfigWindow::draw(bool& show, Configuration& config, bool& readFromSerial, bool& readFromNetwork, int& serialPortNumber, int& portNumber) {
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));



	ImGui::Begin("Config", &show);
	if(m_onStart){
		m_infoBackUp = Color::Info;
		m_traceBackUp = Color::Trace;
		m_debugBackUp = Color::Debug;
		m_warningBackUp = Color::Warning;
		m_errorBackUp = Color::Error;
		m_onStart = false;
	}

	if (ImGui::CollapsingHeader("Colors")) {
		ImGui::Text("Colors HERE");
		ImGui::ColorEdit3("TRACE", (float*)&Color::Trace);
		ImGui::ColorEdit3("INFO", (float*)&Color::Info);
		ImGui::ColorEdit3("DEBUG", (float*)&Color::Debug);
		ImGui::ColorEdit3("WARNING", (float*)&Color::Warning);
		ImGui::ColorEdit3("ERROR", (float*)&Color::Error);

		if(ImGui::Button("Discard")) {
			Color::Info = m_infoBackUp;
			Color::Trace = m_traceBackUp;
			Color::Debug = m_debugBackUp;
			Color::Warning = m_warningBackUp;
			Color::Error = m_errorBackUp;
			m_onStart = true;
		}

		ImGui::SameLine();

		if (ImGui::Button("Set Default Colors")) {
			Color::Error = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);
			Color::Trace = ImVec4(1.0f, 0.5f, 0.0f, 1.0f);
			Color::Debug = ImVec4(0.0f, 1.0f, 1.0f, 1.0f);
			Color::Info = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);
			Color::Warning = ImVec4(1.0f, 0.0f, 1.0f, 1.0f);
		}
	}

	if (ImGui::CollapsingHeader("Port Number")) {
		ImGui::InputInt("Set port Number", &portNumber);
		ImGui::SameLine();
		ImGui::Checkbox("Read from TCP/IP", &readFromNetwork );
	}

	if (ImGui::CollapsingHeader("Serial Port")) {
		ImGui::Text("Choose serial port");
		
		ImGui::Combo("serial port number", &serialPortNumber, "COM1\0COM2\0COM3\0COM4\0COM5\0COM6\0COM7\0COM8\0\0");
		//ImGui::Text("%d", item_current_2);
		ImGui::SameLine();
		ImGui::Checkbox("Read from serial", &readFromSerial);
	}

	if (ImGui::CollapsingHeader("Default Path")) {
		ImGui::Text("Default Path: ");
		ImGui::SameLine();
		ImGui::Text(m_defaultPath.c_str());
		if(m_showDefaultPathPicker)defaultPath();
		if(ImGui::Button("Choose default Path")){
			m_showDefaultPathPicker = true;
		}
	}

	
	if (ImGui::Button("Save")) {
		config.save(m_defaultPath, portNumber);
	}
	ImGui::SameLine();
	if (ImGui::Button("Close")) {
		show = false;
		m_onStart = true;
	}



	ImGui::End();

	ImGui::PopStyleColor();
}


void ImGuiConfigWindow::defaultPath() {
	ImGuiFileDialog::Instance()->OpenDialog("ChoosePath", "Choose Path", ".*\0.txt\0.log\0.h\0.hpp\0\0", ".");

	// display
	if (ImGuiFileDialog::Instance()->FileDialog("ChoosePath"))
	{
		// action if OK
		if (ImGuiFileDialog::Instance()->IsOk == true)
		{
			//m_defaultPath = ImGuiFileDialog::Instance()->GetCurrentFileName();
			m_defaultPath = ImGuiFileDialog::Instance()->GetFilepathName();
			//m_defaultPath = ImGuiFileDialog::Instance()->GetCurrentPath();

			// action
		}
		// close
		ImGuiFileDialog::Instance()->CloseDialog("ChoosePath");
		m_showDefaultPathPicker = false;
	}
}