#pragma once
#include "imgui/imgui.h"
#include "Colors.h"
#include "Configuration.h"
#include "imgui/ImGuiFileDialog.h"

class ImGuiConfigWindow
{
public :
	void draw(bool& show, Configuration& config, bool& readFromSerial, bool& readFromNetwork, int& serialPortNumber, int& portNumber);
	ImGuiConfigWindow(Configuration& config);
private:

	static bool m_showDefaultPathPicker;

	ImVec4 m_traceBackUp;
	ImVec4 m_infoBackUp;
	ImVec4 m_debugBackUp;
	ImVec4 m_warningBackUp;
	ImVec4 m_errorBackUp;

	bool m_onStart = true;

	std::string m_defaultPath;

	void defaultPath();

};

