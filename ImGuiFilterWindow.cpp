#include "ImGuiFilterWindow.h"


ImGuiFilterWindow::ImGuiFilterWindow(std::deque<Log>& logs, std::deque<Log>& filteredLog, Filter& filter) : m_logs(logs), m_filteredLog(filteredLog), m_filter(filter) {
}

void ImGuiFilterWindow::draw(bool& showFilter, bool& printFiltered) {
	static bool useSeverity = false;

	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));
	ImGui::Begin("Filter", &showFilter);
	//getUniqueSources();
	if(ImGui::CollapsingHeader("Severities")){
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		ImGui::Checkbox("INFO", &m_filter.m_showInfo);
		ImGui::Checkbox("TRACE", &m_filter.m_showTrace);
		ImGui::Checkbox("DEBUG", &m_filter.m_showDebug);
		ImGui::Checkbox("WARNING", &m_filter.m_showWarning);
		ImGui::Checkbox("ERROR", &m_filter.m_showError);
		//ImGui::SameLine();
		//ImGui::Checkbox("Use", &useSeverity);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (ImGui::CollapsingHeader("Text Filter")) {
		if (ImGui::Button("Add filter")) {
			if(m_filter.m_numOfTextFilters < 11){
				m_filter.m_numOfTextFilters++;
			}
			//m_filterText.push_back();
		}
		ImGui::SameLine();
		if (ImGui::Button("Delete filter")) {
			if(m_filter.m_numOfTextFilters > 0){
				m_filter.m_numOfTextFilters--;
			}
			//m_filterText.push_back();
			}

		if (m_filter.m_numOfTextFilters >= 1) ImGui::InputText("1", m_filter.m_filter1, IM_ARRAYSIZE(m_filter.m_filter1));
		if (m_filter.m_numOfTextFilters >= 2) ImGui::InputText("2", m_filter.m_filter2, IM_ARRAYSIZE(m_filter.m_filter2));
		if (m_filter.m_numOfTextFilters >= 3) ImGui::InputText("3", m_filter.m_filter3, IM_ARRAYSIZE(m_filter.m_filter3));
		if (m_filter.m_numOfTextFilters >= 4) ImGui::InputText("4", m_filter.m_filter4, IM_ARRAYSIZE(m_filter.m_filter4));
		if (m_filter.m_numOfTextFilters >= 5) ImGui::InputText("5", m_filter.m_filter5, IM_ARRAYSIZE(m_filter.m_filter5));
		if (m_filter.m_numOfTextFilters >= 6) ImGui::InputText("6", m_filter.m_filter6, IM_ARRAYSIZE(m_filter.m_filter6));
		if (m_filter.m_numOfTextFilters >= 7) ImGui::InputText("7", m_filter.m_filter7, IM_ARRAYSIZE(m_filter.m_filter7));
		if (m_filter.m_numOfTextFilters >= 8) ImGui::InputText("8", m_filter.m_filter8, IM_ARRAYSIZE(m_filter.m_filter8));
		if (m_filter.m_numOfTextFilters >= 9) ImGui::InputText("9", m_filter.m_filter9, IM_ARRAYSIZE(m_filter.m_filter9));
		if (m_filter.m_numOfTextFilters >= 10) ImGui::InputText("10", m_filter.m_filter10, IM_ARRAYSIZE(m_filter.m_filter10));

	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if (ImGui::Button("Apply")) {
		/*
		filterLogsBySeverity();

		if (m_numOfFilters >= 1) { filterLogsByText(m_filter.m_filter1); }
		if (m_numOfFilters >= 2) { filterLogsByText(m_filter.m_filter2); }
		if (m_numOfFilters >= 3) { filterLogsByText(m_filter.m_filter3); }
		if (m_numOfFilters >= 4) { filterLogsByText(m_filter.m_filter4); }
		if (m_numOfFilters >= 5) { filterLogsByText(m_filter.m_filter5); }
		if (m_numOfFilters >= 6) { filterLogsByText(m_filter.m_filter6); }
		if (m_numOfFilters >= 7) { filterLogsByText(m_filter.m_filter7); }
		if (m_numOfFilters >= 8) { filterLogsByText(m_filter.m_filter8); }
		if (m_numOfFilters >= 9) { filterLogsByText(m_filter.m_filter9); }
		if (m_numOfFilters >= 10) { filterLogsByText(m_filter.m_filter10); }
		*/
		m_filteredLog.clear();
		for (Log l : m_logs) {
			if (m_filter.filterBySeverity(l) && m_filter.filterByText(m_filter.m_numOfTextFilters, l)) {
				m_filteredLog.push_back(l);
			}
		}
		showFilter = false; 
		printFiltered = true;
	}
	ImGui::SameLine();
	if (ImGui::Button("Hide")) {
		showFilter = false;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	ImGui::End(); // Filter

	ImGui::PopStyleColor();
}

//TODO needs some work
void ImGuiFilterWindow::filterLogsBySeverity() {

	for each(Log l in m_logs) {
		if (m_filter.filterBySeverity(l)) {
			m_filteredLog.push_back(l);
		}
	}
}

void ImGuiFilterWindow::filterLogsByText( ) {
	for (Log l : m_filteredLog) {
		std::cout << "Going Throught logs" << std::endl;
		if (m_filter.filterByText(m_filter.m_numOfTextFilters, l)) {
			m_filteredLog.push_back(l);
		}
	}
}

Filter ImGuiFilterWindow::getFiter() {
	return m_filter;
}