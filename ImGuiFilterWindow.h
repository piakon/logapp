#pragma once
#include <deque>
#include <vector> 
#include <algorithm> 
#include "Log.h"
#include "imgui/imgui.h"
#include <regex>
#include "Filter.h"

class ImGuiFilterWindow
{
public:
	ImGuiFilterWindow(std::deque<Log>& logs, std::deque<Log>& filteredLog, Filter& filter);

	void draw(bool& showFilter, bool& printFiltered);

	Filter getFiter();

private:
	void filterLogsBySeverity();
	void filterLogsByText();

	Filter& m_filter;


	std::deque<Log>& m_logs;
	std::deque<Log>& m_filteredLog;

	//static int m_numOfFilters;
};

