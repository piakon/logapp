#include "ImGuiLogDetailWindow.h"

Log ImGuiLogDetailWindow::m_log = Log();


ImGuiLogDetailWindow::ImGuiLogDetailWindow(){

}


void ImGuiLogDetailWindow::draw(bool& showThis) {
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));

	ImGui::Begin("Log Detail", &showThis);

	ImGui::Text("Type : ");
	ImGui::SameLine();
	ImGui::Text(m_log.type.c_str());

	ImGui::Text("Logger : ");
	ImGui::SameLine();
	ImGui::Text(m_log.logger.c_str());

	ImGui::Text("Level : ");
	ImGui::SameLine();
	ImGui::Text(m_log.level.c_str());

	ImGui::Text("Class : ");
	ImGui::SameLine();
	ImGui::Text(m_log.clazz.c_str());

	ImGui::Text("Method : ");
	ImGui::SameLine();
	ImGui::Text(m_log.method.c_str());

	ImGui::Text("Message : ");
	ImGui::SameLine();
	ImGui::Text(m_log.message.c_str());

	ImGui::Text("File : ");
	ImGui::SameLine();
	ImGui::Text(m_log.file.c_str());

	ImGui::Text("srcIp : " );
	ImGui::SameLine();
	ImGui::Text(m_log.srcIp.c_str());

	ImGui::Text("Line : ");
	ImGui::SameLine();
	ImGui::Text(std::to_string(m_log.line).c_str());

	ImGui::Text("localTime : ");
	ImGui::SameLine();
	ImGui::Text(m_log.date.c_str());

	if(ImGui::Button("Hide")) {
		showThis = false;
	}

	ImGui::End();
	
	ImGui::PopStyleColor();
}

void ImGuiLogDetailWindow::setLog(Log& log) {
	m_log = log;
}

/*     std::string type = "";
     std::string date = "";
     std::string logger = "";
     std::string level = "";
     std::string clazz = "";
     std::string method = "";
     std::string message = "";
     std::string file = "";
     int line = 0;
     std::string srcIp = "";
     time_t localTime;*/