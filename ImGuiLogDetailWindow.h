#pragma once
#include "Log.h"
#include "imgui/imgui.h"
#include <string>
#include <ctime>

class ImGuiLogDetailWindow
{
public:
	ImGuiLogDetailWindow();

	static Log m_log;

	void draw(bool& showThis);

	static void setLog(Log& log);
private:


};

