#include "ImGuiLogPrinter.h"

       

ImGuiLogPrinter::ImGuiLogPrinter(std::deque<Log>& logs, std::deque<Log>& filteredLogs, bool& printFiltered) : m_logs(logs), m_filteredLogs(filteredLogs), m_printFiltered(printFiltered)
	{
		AutoScroll = true;
		Clear();
	}

	void    ImGuiLogPrinter::Clear()
	{
		Buf.clear();
		LineOffsets.clear();
		LineOffsets.push_back(0);
		m_logs.clear();
	}
	
	void    ImGuiLogPrinter::AddLog(const char* fmt, ...) IM_FMTARGS(2)
	{
		int old_size = Buf.size();
		va_list args;
		va_start(args, fmt);
		Buf.appendfv(fmt, args);
		va_end(args);
		for (int new_size = Buf.size(); old_size < new_size; old_size++)
			if (Buf[old_size] == '\n')
				LineOffsets.push_back(old_size + 1);
	}

	void    ImGuiLogPrinter::Draw(bool& printFiltered, bool& showDetailedInfo)
	{
		// Options menu
		if (ImGui::BeginPopup("Options"))
		{
			ImGui::Checkbox("Auto-scroll", &AutoScroll);
			ImGui::EndPopup();
		}

		ImGui::SameLine();
		bool clear = ImGui::Button("Clear");
		ImGui::SameLine();

		ImGui::Separator();
		ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);

		if (clear)
			Clear();
		//if (copy)
		//	ImGui::LogToClipboard();

		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
		const char* buf = Buf.begin();
		const char* buf_end = Buf.end();
		if (printFiltered)
		{
			// In this example we don't use the clipper when Filter is enabled.
			// This is because we don't have a random access on the result on our filter.
			// A real application processing logs with ten of thousands of entries may want to store the result of search/filter.
			// especially if the filtering function is not trivial (e.g. reg-exp).
			ImGuiListClipper clipper(m_filteredLogs.size());
			//Color ColorPicker;
			while (clipper.Step())
			{
				for (int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
				{
					Log log = m_filteredLogs.at(line_no);


					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
					ImGui::PushStyleColor(ImGuiCol_Text, pickColor(log));
					//ImGui::TextUnformatted(m_logs.at(line_no).returnLog().c_str());
					if (ImGui::Button(m_filteredLogs.at(line_no).returnLog().c_str())) {
						ImGuiLogDetailWindow::setLog(log);
						showDetailedInfo = true;
					}
					ImGui::PopStyleColor();
					ImGui::PopStyleColor();
				}
			}
			clipper.End();
		}
		else
		{
			// The simplest and easy way to display the entire buffer:
			//   ImGui::TextUnformatted(buf_begin, buf_end);
			// And it'll just work. TextUnformatted() has specialization for large blob of text and will fast-forward to skip non-visible lines.
			// Here we instead demonstrate using the clipper to only process lines that are within the visible area.
			// If you have tens of thousands of items and their processing cost is non-negligible, coarse clipping them on your side is recommended.
			// Using ImGuiListClipper requires A) random access into your data, and B) items all being the  same height,
			// both of which we can handle since we an array pointing to the beginning of each line of text.
			// When using the filter (in the block of code above) we don't have random access into the data to display anymore, which is why we don't use the clipper.
			// Storing or skimming through the search result would make it possible (and would be recommended if you want to search through tens of thousands of entries)
			//ImGuiListClipper clipper(LineOffsets.Size);
			ImGuiListClipper clipper(m_logs.size());
			//Color ColorPicker;
			while (clipper.Step())
			{
				for (int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
				{

					Log log = m_logs.at(line_no);

					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
					ImGui::PushStyleColor(ImGuiCol_Text, pickColor(log));
					//ImGui::TextUnformatted(m_logs.at(line_no).returnLog().c_str());
					if (ImGui::Button(m_logs.at(line_no).returnLog().c_str())) {
						ImGuiLogDetailWindow::setLog(log);
						showDetailedInfo = true;
					}
					ImGui::PopStyleColor();
					ImGui::PopStyleColor();
				}
			}
			clipper.End();
		}
		ImGui::PopStyleVar();

		if (AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
			ImGui::SetScrollHereY(1.0f);

		ImGui::EndChild();

	}


ImVec4	ImGuiLogPrinter::pickColor(Log log) {
	if (log.level.find("TRACE") != std::string::npos) return Color::Trace;
	if (log.level.find("INFO") != std::string::npos) return Color::Info;
	if (log.level.find("DEBUG") != std::string::npos) return Color::Debug;
	if (log.level.find("WARN") != std::string::npos) return Color::Warning;
	if (log.level.find("ERROR") != std::string::npos) return Color::Error;
	return ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
	}