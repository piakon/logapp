#pragma once
#include "imgui/imgui.h"
#include "Colors.h"
#include "Log.h"
#include <deque>
#include"ImGuiLogDetailWindow.h"


class ImGuiLogPrinter
{
	ImGuiTextBuffer     Buf;
	ImGuiTextFilter     Filter;
	ImVector<int>       LineOffsets;     // Index to lines offset. We maintain this with AddLog() calls, allowing us to have a random access on lines
	bool                AutoScroll;     // Keep scrolling if already at the bottom

	std::deque<Log>& m_logs;
	std::deque<Log>& m_filteredLogs;
	bool& m_printFiltered;
public:
	ImGuiLogPrinter(std::deque<Log>& logs, std::deque<Log>& filteredLogs, bool& printFiltered);


	void    Clear();


	void    AddLog(const char* fmt, ...) IM_FMTARGS(2);

		
	void    Draw(bool& printFiltered, bool& showDetailedInfo);

	ImVec4 pickColor(Log log);

};

