#include "ImGuiWindow.h"

bool MyImGuiWindow::m_printed = false;
bool MyImGuiWindow::m_printFiltered = false;
bool MyImGuiWindow::m_showFilterDialog = false;
bool MyImGuiWindow::m_showFileDialog = false;
bool MyImGuiWindow::m_showConfigWindow = false;
bool MyImGuiWindow::m_showDetailedInfo = false;
bool MyImGuiWindow::m_readFromSerial = false;
bool MyImGuiWindow::m_readFromNetwork = false;
bool MyImGuiWindow::m_loadingFromFile = false;
bool MyImGuiWindow::m_showSaveDialog = false;
bool MyImGuiWindow::m_loadLines = true;

int MyImGuiWindow::m_numOfLoaded = 0;

int MyImGuiWindow::m_serialPortNumber = 0;
int MyImGuiWindow::m_portNumber = 00000;

unsigned int MyImGuiWindow::m_logs_size = 0;
//ImGuiLogPrinter MyImGuiWindow::m_logPrinter = ImGuiLogPrinter(m_logs);

MyImGuiWindow::MyImGuiWindow():
m_logPrinter(m_logs, m_filteredLogs, m_printFiltered),
m_filterWindow(m_logs, m_filteredLogs, m_filter),
m_config("config.ini"),
m_logDetailWindow(),
m_configWindow(m_config),
m_interpreter(m_logs, m_filteredLogs, m_filter)
{	
	MyImGuiWindow::m_logs_size = m_logs.size();
	m_config.setColors();
}

void MyImGuiWindow::window() {

	static bool loggerActive = true;
	static int counter = 0;

	if (MyImGuiWindow::m_showFileDialog) loadfileDialog();
	if (MyImGuiWindow::m_showFilterDialog)  m_filterWindow.draw(m_showFilterDialog,  m_printFiltered);
	if (MyImGuiWindow::m_showConfigWindow)  m_configWindow.draw(m_showConfigWindow, m_config, m_readFromSerial, m_readFromNetwork, m_serialPortNumber, m_portNumber);
	if (MyImGuiWindow::m_showDetailedInfo) m_logDetailWindow.draw(m_showDetailedInfo);
	if (MyImGuiWindow::m_printFiltered && MyImGuiWindow::m_showSaveDialog) saveIntoFileDialog(m_filteredLogs);
	if (!MyImGuiWindow::m_printFiltered && MyImGuiWindow::m_showSaveDialog) saveIntoFileDialog(m_logs);
	if (MyImGuiWindow::m_readFromNetwork) {
		if(m_interpreter.loaderEmpty()){
			m_interpreter.setLoader(std::make_unique<NetworkLoader>(m_portNumber, 1, m_readFromNetwork));
		}
	}

	ImGui::SetNextWindowPos(ImVec2(.0f, .0f));
	if (!ImGui::Begin("LogPrinter",&loggerActive, ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoBringToFrontOnFocus)) // Logger Window
	{
		ImGui::End();
		return;
	}

	if (ImGui::BeginMenuBar()) {
		if (ImGui::BeginMenu("File")) {
			if (ImGui::MenuItem("Open")) {
				MyImGuiWindow::m_showFileDialog = true;
			}
			if (ImGui::MenuItem("Discard")) {
				m_logs.clear();
				m_filteredLogs.clear();
			}
			if (ImGui::MenuItem("Save displayed logs")) {
				MyImGuiWindow::m_showSaveDialog = true;
			}
			ImGui::EndMenu();

		}
		if (ImGui::BeginMenu("Filter")) {
			if (ImGui::MenuItem("Select filter")) {
				MyImGuiWindow::m_showFilterDialog = true;
			}
			if (ImGui::MenuItem("Drop filter")) {
				m_filteredLogs.clear();
				m_printFiltered = false;
				m_filterWindow.getFiter().m_showInfo = true;
				m_filterWindow.getFiter().m_showTrace = true;
				m_filterWindow.getFiter().m_showDebug = true;
				m_filterWindow.getFiter().m_showWarning = true;
				m_filterWindow.getFiter().m_showError = true;
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Configuration")) {
			if (ImGui::MenuItem("Show Configuration")) {
				MyImGuiWindow::m_showConfigWindow = true;
			}
			if (ImGui::MenuItem("Delete Configuration")) {
			// reset configuration
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Stop / continue loading")) {
			if (ImGui::MenuItem("Stop")) {
				MyImGuiWindow::m_loadLines = false;
			}
			if (ImGui::MenuItem("Continue")) {
				MyImGuiWindow::m_loadLines = true;
			}
			/*
			if (ImGui::MenuItem("Reset")) {
				m_interpreter.resetLoaderAndParser();
			}
			*/
			ImGui::EndMenu();
		}

		ImGui::EndMenuBar();
	}
	if(MyImGuiWindow::m_loadLines){
		m_interpreter.readLines();
	}
	m_logPrinter.Draw(m_printFiltered, m_showDetailedInfo);

	ImGui::Separator();
	ImGui::NewLine();
	ImGui::End(); // LOGGER Window
	//ImGui::End(); // LOG READING APP
}

void MyImGuiWindow::loadfileDialog() {
	ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".*\0.txt\0.log\0.h\0.hpp\0\0", m_config.Get("PATH", "DEFAULT_PATH", "."));

	// display
	if (ImGuiFileDialog::Instance()->FileDialog("ChooseFileDlgKey"))
	{
		// action if OK
		if (ImGuiFileDialog::Instance()->IsOk == true)
		{
			MyImGuiWindow::m_numOfLoaded = 0;
			MyImGuiWindow::m_loadingFromFile = true;
			std::string ALLPATH = ImGuiFileDialog::Instance()->GetFilepathName();
			m_logs.clear();
			if (m_interpreter.loaderEmpty()) {
				m_interpreter.setLoader(std::make_unique<FileLoader>(ALLPATH));
			}
			//m_interpreter.readLines();
			//m_fileLoader.loadFile(ALLPATH, m_numOfLoaded);
			MyImGuiWindow::m_printed = false;

			// action
		}
		// close
		ImGuiFileDialog::Instance()->CloseDialog("ChooseFileDlgKey");
		MyImGuiWindow::m_loadingFromFile = false;
		MyImGuiWindow::m_showFileDialog = false;

	}
}

void MyImGuiWindow::saveIntoFileDialog(std::deque<Log> logs) {
	ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKeySave", "Save Logs", ".*\0.txt\0.log\0.h\0.hpp\0\0", m_config.Get("PATH", "DEFAULT_PATH", "."));

	// display
	if (ImGuiFileDialog::Instance()->FileDialog("ChooseFileDlgKeySave"))
	{
		// action if OK
		if (ImGuiFileDialog::Instance()->IsOk == true)
		{

			MyImGuiWindow::m_loadingFromFile = true;
			std::string ALLPATH = ImGuiFileDialog::Instance()->GetFilepathName();
			m_interpreter.saveLogs(ALLPATH, logs);
			MyImGuiWindow::m_printed = false;

			// action
		}
		// close
		ImGuiFileDialog::Instance()->CloseDialog("ChooseFileDlgKeySave");
		MyImGuiWindow::m_showSaveDialog = false;

	}
}
