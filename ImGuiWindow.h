#pragma once
#include "imgui/imgui.h"
#include "ImGuiLogPrinter.h"
#include "LogInterpreter.h"
#include "NetworkLoader.h"
#include "FileLoader.h"

#include "imgui/ImGuiFileDialog.h"
#include "ImGuiFilterWindow.h"
#include "ImGuiConfigWindow.h"
#include "Configuration.h"
#include "ImGuiLogDetailWindow.h"

#include <thread>


class MyImGuiWindow
{
public:
	static bool m_showFileDialog;
	static bool m_showFilterDialog;
	static bool m_printFiltered;
	static bool m_showConfigWindow;
	static bool m_showDetailedInfo;
	static bool m_readFromSerial;
	static bool m_readFromNetwork;
	static bool m_loadingFromFile;
	static bool m_showSaveDialog;
	static bool m_loadLines;

	static int m_numOfLoaded;
	static int m_serialPortNumber;
	static int m_portNumber;

	static unsigned int m_logs_size;
private:
	LogInterpreter m_interpreter;
	std::deque<Log> m_logs;
	std::deque<Log> m_filteredLogs;

	Filter m_filter;

	static bool m_printed;


	Configuration m_config;

    ImGuiLogPrinter m_logPrinter;

	ImGuiFilterWindow m_filterWindow;

	ImGuiConfigWindow m_configWindow;

	ImGuiLogDetailWindow m_logDetailWindow;

	//void readFromNetwork();

public:
	MyImGuiWindow();

	void window();

	void loadfileDialog();

	void saveIntoFileDialog(std::deque<Log> logs);

};

