//
// Created by Ondra on 10.03.2020.
//

#ifndef APP_LOG_H
#define APP_LOG_H

#include <string>
#include <regex>
#include <iostream>
#include <map>


struct Log  {
     std::string type = "";
     std::string date = "";
     std::string logger = "";
     std::string level = "";
     std::string clazz = "";
     std::string method = "";
     std::string message = "";
     std::string file = "";
     int line = 0;
     std::string srcIp = "";
     time_t localTime;
     std::map<std::string, std::string> others;

    void printAll(){
         std::cout<< type << " " << date << " " << logger << " " << level << " " << clazz << " " << method << " " << message << " " << file << " " << line << " "<< srcIp << " " << localTime << std::endl;
     }

	std::string returnLog() {
		return type + " " + date + " " + logger + " " + level + " " + clazz + " " + method + " " + message + " " + file + " " + std::to_string(line) + " " + srcIp + " " + std::to_string(localTime) + "\n";
	}

     bool contain(std::string& reg){
        if(type.find(reg) != std::string::npos) return true;
        if(date.find(reg) != std::string::npos) return true;
        if(logger.find(reg) != std::string::npos) return true;
        if(level.find(reg) != std::string::npos) return true;
        if(clazz.find(reg) != std::string::npos) return true;
        if(method.find(reg) != std::string::npos) return true;
        if(message.find(reg) != std::string::npos) return true;
        if(file.find(reg) != std::string::npos) return true;
        //if(line == std::stoi(reg) != std::string::npos) return true;
        if(srcIp.find(reg) != std::string::npos) return true;
        return false;
     }
};


#endif //APP_LOG_H
