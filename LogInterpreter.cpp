#include "LogInterpreter.h"

LogInterpreter::LogInterpreter(std::deque<Log>& logs, std::deque<Log>& filteredLogs, Filter& filter) : m_filter(filter) , m_logs(logs), m_parserFactory(), m_filteredLogs(filteredLogs), m_saver(std::make_unique<HypercomSaver>()) {
}

void LogInterpreter::readLines() {
	if (m_loader.get() == nullptr) {
		return;
	}
	std::vector<std::string> lines = m_loader->returnLines();
	if (m_parser.get() == nullptr && !lines.empty()) {
		std::string line = lines.front();
		if(!line.empty()){
			m_parser = m_parserFactory.createParser(line);
		}
	}
	if(!lines.empty()){

		for (std::string s : lines) {
			Log l = m_parser->parse(s);
			m_logs.push_back(l);
			if (m_filter.filterBySeverity(l) && m_filter.filterByText(m_filter.m_numOfTextFilters,l)) {
				m_filteredLogs.push_back(l);
			}
		}
	}
	
}

bool LogInterpreter::loaderEmpty() {
	return m_loader.get() == nullptr;
}

void LogInterpreter::setLoader(std::unique_ptr<ILoader> loader) {
	m_loader = std::move(loader);
}

void LogInterpreter::saveLogs(std::string path, std::deque<Log> logs) {
	m_saver->saveLogs(path, logs);
}
/*
void LogInterpreter::resetLoaderAndParser() {
	m_loader.reset();
	m_parser.reset();
}
*/