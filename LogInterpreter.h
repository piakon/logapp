#pragma once
#include <deque>


#include "ILoader.h"
#include "IParser.h"
#include "Log.h"
#include "ParserFactory.h"
#include "Filter.h"
#include "ISaver.h"
#include "HypersomSaver.h"

class LogInterpreter
{
public:
	LogInterpreter(std::deque<Log>& logs, std::deque<Log>& filteredLogs, Filter& filter);

	void readLines();

	void setLoader(std::unique_ptr<ILoader> loader);

	bool loaderEmpty();

	//void resetLoaderAndParser();

	void saveLogs(std::string path, std::deque<Log> logs);

private:
	Filter& m_filter;

	std::unique_ptr<ISaver> m_saver;

	std::deque<Log>& m_logs;
	std::deque<Log>& m_filteredLogs;

	std::unique_ptr<ILoader> m_loader;
	std::unique_ptr<IParser> m_parser;

	ParserFactory m_parserFactory;
};

