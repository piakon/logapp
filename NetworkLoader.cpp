#include "NetworkLoader.h"

NetworkLoader::NetworkLoader(int port, int ip ,bool& read) : ILoader(), m_ip(ip), m_port(port) , m_read(read) {
	
	thread1 = std::thread(&NetworkLoader::read, this);

}

NetworkLoader::~NetworkLoader() {
	if (thread1.joinable()) thread1.join();
}


void  NetworkLoader::openSocket(int port, int ip) {
	WSADATA wsaData;
	int iResult;

	if(clientSocket == INVALID_SOCKET){
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed with error: %d\n", iResult);
			return ;
		}

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;


		iResult = getaddrinfo(NULL, std::to_string(port).c_str() , &hints, &result);
		if (iResult != 0) {
			printf("getaddrinfo failed: %d\n", iResult);
			WSACleanup();
			return ;
		}

		listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
		if (listenSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			freeaddrinfo(result);
			WSACleanup();
			return ;
		}

		iResult = bind(listenSocket, result->ai_addr, (int)result->ai_addrlen);
		if (iResult == SOCKET_ERROR)
		{
			printf("bind failed with error: %d\n", WSAGetLastError());
			freeaddrinfo(result);
			closesocket(listenSocket);
			WSACleanup();
			return ;
		}

		freeaddrinfo(result);

		iResult = listen(listenSocket, SOMAXCONN);
		if (iResult == SOCKET_ERROR) {
			printf("listen failed with error: %d\n", WSAGetLastError());
			closesocket(listenSocket);
			WSACleanup();
			return ;
		}

		int iResult = 0;
		if (m_read) {
			clientSocket = accept(listenSocket, NULL, NULL);
			if (clientSocket == INVALID_SOCKET) {
				printf("accept failed: %d\n", WSAGetLastError());
				closesocket(listenSocket);
				WSACleanup();
				return;
			}
			do {
				char recvbuf[DEFAULT_BUFLEN] = {};
				int recvbuflen = DEFAULT_BUFLEN;
				iResult = recv(clientSocket, recvbuf, recvbuflen, 0);
				m_notAppended = true;
				if (iResult > 0) {
					do {
						if (m_notAppended) {
							m_loaded.append(convertToString(recvbuf, sizeof(recvbuf) / sizeof(char))); //std::to_string(recvbuf); 
							m_notAppended = false;
						}
						auto pos = m_loaded.find("\n", 0);
						std::string line = m_loaded.substr(0, pos);
						m_loaded.erase(0, pos + 1);
						//printf("Bytes received: %d\n", iResult);
						//std::cout << "text : " << s << std::endl;;


						m_mutexVector.lock();
						m_strings.push_back(line);
						m_mutexVector.unlock();


						if (m_loaded.find("\n", 0) != std::string::npos) {
							m_nextNewLineExist = true;
						}
						else {
							m_nextNewLineExist = false;
						}
					} while (m_nextNewLineExist);
				}
			} while (iResult > 0);
		}
		return;
	}
	return ;
}


bool NetworkLoader::closeSocket() {
	return false;
}

void NetworkLoader::read() { // -> std::vector<std::string>& 
	openSocket(m_port, m_ip);
}

std::string NetworkLoader::convertToString(char* a, int size)
{
	int i;
	std::string s = "";
	for (i = 0; i < size; i++) {
		s = s + a[i];
		if (a[i] == NULL) {
			return s;
		}
	}
	return s;
}

std::vector<std::string> NetworkLoader::returnLines() {
	std::vector<std::string> m_lines;
	if (!m_strings.empty()) {
		m_mutexVector.lock();
		m_lines.swap(m_strings);
		m_mutexVector.unlock();
	}
	return m_lines;
}