#pragma once

#undef UNICODE

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#undef _WINSOCKAPI_
#define _WINSOCKAPI_
#include <winsock2.h>
#include <ws2tcpip.h>
#include <Windows.h>
#include <stdlib.h>
#include <stdio.h>


// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")


#include <iostream>
#include <string>
#include <vector>

#include <mutex>

#define DEFAULT_BUFLEN 512
//#define DEFAULT_PORT "27015"
#define IP_PROTOCOL 0

#include "ILoader.h"

class NetworkLoader : public ILoader
{
public:
	NetworkLoader(int port, int ip, bool& read);
	~NetworkLoader();

	void openSocket(int port, int ip);
	bool closeSocket();


	 void read();
	 std::vector<std::string> returnLines();


private:

	int m_port;
	int m_ip;

	SOCKET listenSocket = INVALID_SOCKET;
	SOCKET clientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	std::string convertToString(char* a, int size);

	bool& m_read;

	bool m_nextNewLineExist = false;
	bool m_notAppended = true;
	std::string m_loaded;

	std::vector<std::string> m_strings;

	std::thread thread1;

	std::mutex m_mutexVector;

	
		
};