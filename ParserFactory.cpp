#include "ParserFactory.h"

ParserFactory::ParserFactory() {}

std::unique_ptr<IParser> ParserFactory::createParser(std::string line) {
	if (std::regex_search(line, ACE_REGEX)) return createAceParser();

	if (std::regex_search(line, HYPERCOM_REGEX)) return createHypercomParser();

	if (std::regex_search(line, PLAINTEXT_REGEX)) return createPlainTextParser();

	return nullptr;
}

std::unique_ptr<PlainTextParser> ParserFactory::createPlainTextParser() {
	return std::make_unique<PlainTextParser>();
}
std::unique_ptr<AceParser> ParserFactory::createAceParser() {
	return std::make_unique<AceParser>();
}
std::unique_ptr<HypercomParser> ParserFactory::createHypercomParser() {
	return std::make_unique<HypercomParser>();
}