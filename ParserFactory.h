#pragma once
#include "IParser.h"
#include "PlainTextParser.h"
#include "HypercomParser.h"
#include "AceParser.h"
#include <regex>
#include "HypercomLoggingConstants.h"

class ParserFactory
{
public:
	ParserFactory();

	std::unique_ptr<IParser> createParser(std::string line);

private:
	std::unique_ptr<PlainTextParser> createPlainTextParser();
	std::unique_ptr<AceParser> createAceParser();
	std::unique_ptr<HypercomParser> createHypercomParser();

	const std::regex ACE_REGEX = std::regex("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3} [A-Z]* .*", std::regex_constants::ECMAScript);
	const std::regex HYPERCOM_REGEX = std::regex(TAG_RECORD_START + ".*" + TAG_RECORD_END, std::regex_constants::ECMAScript);
	const std::regex PLAINTEXT_REGEX = std::regex("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}: .* : .* : .* : */ ( .* : .* )");
};

