#pragma once
#include <regex>

const std::regex TRACE = std::regex("TRACE", std::regex_constants::icase | std::regex_constants::ECMAScript);
const std::regex INFO = std::regex("INFO", std::regex_constants::icase | std::regex_constants::ECMAScript);
const std::regex WARN = std::regex("WARN", std::regex_constants::icase | std::regex_constants::ECMAScript);
const std::regex DEBUG = std::regex("DEBUG", std::regex_constants::icase | std::regex_constants::ECMAScript);
const std::regex ERRORR = std::regex("ERROR", std::regex_constants::icase | std::regex_constants::ECMAScript);